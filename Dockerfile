# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
#FROM node:12-stretch as build-stage
#WORKDIR /app
#COPY package*.json .
#RUN yarn install
#COPY . .
#RUN yarn build
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:latest
# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html
# Remove default nginx static assets
RUN rm -rf ./*
# Copy static assets from builder stage
#COPY --from=build-stage /app/build/ .
COPY /build/ .
# Copy the default nginx.conf provided by tiangolo/node-frontend
COPY  /nginx.conf /etc/nginx/nginx.conf
EXPOSE 80